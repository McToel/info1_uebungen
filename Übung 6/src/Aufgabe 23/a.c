#include <stdio.h>
#include <string.h>

#define SMAX 15

int main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("You must supply at least 2 arguments, not %i\n",
		       argc - 1);
		return 1;
	}

	int i = 1;
	for (; i < argc; i++) {
		if (strcmp(argv[i], argv[i + 1]) > 0) {
			printf("Argument %i is the first argument, that is not in lexicographic order\n",
			       (i + 1));
			i = -1;
			break;
		}
	}
	if (i != -1) {
		printf("All arguments are in lexicographic order\n");
	}

	i = 1;
	char v[SMAX] = "";
	for (; i < argc && strlen(v) + strlen(argv[i]) <= SMAX; i++) {
		strcat(v, argv[i]);
	}

	printf("%s\n", v);
}