#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VMAX 15

int is_prime(int x, int p[])
{
	if (x == 2) {
		return 1;
	}
	if (x == 1) {
		return 0;
	}
	int i;
	for (i = 0; p[i]; i++) {
		if (x % p[i] == 0) {
			return 0;
		}
	}
	return 1;
}

int count_non_zero(int v[], int n)
{
	int i = 0;
	int result = 0;
	for (; i < n; i++) {
		result += (v[i]) ? 1 : 0;
	}
	return result;
}

void print_array(int arr[], int size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]");
}

int main(int argc, char *argv[])
{
	int primes[VMAX];
	int j = 0;
	for (; j < VMAX; j++) {
		primes[j] = 0;
	}

	if (argc != 2) {
		printf("You must supply 1 argument, not %i\n", argc - 1);
		return 1;
	}

	if (strlen(argv[1]) > 0 && argv[1][0] != '0' &&
	    strspn(argv[1], "0123456789") == strlen(argv[1])) {
		int n = atoi(argv[1]);
		if (n < 1) {
			printf("The passed number is to small (%i)\n", n);
			return 1;
		} else if (n > VMAX) {
			printf("The passed number is to big (%i)\n", n);
			return 1;
		}
		int x, i;
		i = 0;
		for (x = 2; x < n; x++) {
			if (is_prime(x, primes)) {
				primes[i] = x;
				i++;
			}
		}
		printf("Der Anteil der ersten %i Primzahlen ",
		       count_non_zero(primes, VMAX));
		print_array(primes, VMAX);
		printf(" an den natuerlichen Zahlen bis zur %i ist: %f\n", n,
		       count_non_zero(primes, VMAX) / (double)n);

	} else {
		printf("Argument could not be parsed as int");
		return 1;
	}
}
