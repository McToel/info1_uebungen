#include <math.h>

int one(int x, int k) {
    if (! x & (1 << k))
        return 1;
    else
        return 0;
}

int two(int n, int m) {
    if (! n % m)
        return 1;
    else
        return 0;
}

int three(double a) {
    return (int)ceil(a);
}