#include <stdio.h>

char *string_copy(char v[], char w[]) {
    int i = 0;
    while (w[i]) {
        v[i] = w[i];
        i++;
    }
    v[i] = 0;
}


int main(void) {

    char src_one[] = "";
    char dest_one[] = "Hello World";

    string_copy(dest_one, src_one);
    printf("%s\n", dest_one);


    char src_two[] = "Hallo schöne Welt";
    char dest_two[] = "Hello World";

    string_copy(dest_two, src_two);
    printf("%s\n", dest_two);


    char src_three[] = "Hallo";
    char dest_three[] = "Hello World";

    string_copy(dest_three, src_three);
    printf("%s\n", dest_three);
}