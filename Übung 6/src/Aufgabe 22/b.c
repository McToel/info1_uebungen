#include <stdio.h>

int string_indexof(char v[], char c)
{
	int i = 0;
	for (; v[i]; i++) {
		if (v[i] == c) {
			return i;
		}
	}
	return -1;
}

int main(void)
{
	printf("%i\n", string_indexof("", 'c'));
	printf("%i\n", string_indexof("Hello World", 'r'));
	printf("%i\n", string_indexof("Hello World", 'o'));
	printf("%i\n", string_indexof("Hello World", 'z'));
	printf("%i\n", string_indexof("Hello World", ' '));
}