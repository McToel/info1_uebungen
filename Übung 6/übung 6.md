# Übungsblatt 6
## Aufgabe 21
### b)
```c
#include <math.h>

int one(int x, int k) {
    if (! x & (1 << k))
        return 1;
    else
        return 0;
}

int two(int n, int m) {
    if (! n % m)
        return 1;
    else
        return 0;
}

int three(double a) {
    return (int)ceil(a);
}
```

### c)
```c
#include <stdio.h>

int n;
int v[5];
int x;

void one(int n)
{
	switch (n) {
	case 0:
		printf("false\n");
		break;
	case 1:
	case -1:
		printf("unclear\n");
		break;

	default:
		printf("true\n");
		break;
	}
}

void two()
{
	int i = 0;
	int summe = 0;
	while (i < n) {
		summe += v[i];
		i++;
	}
}

void three()
{
	int i = 0;
	printf("%i\n", v[i]);
	i++;
	while (i < n) {
		printf("%i\n", v[i]);
		i++;
	}
}

void four() {
    int i = 1;
    for (; i != n; i++) {
        x -= f(x) / dfdx(x);
    }
}
```

## Aufgabe 22
### a)
```c
#include <stdio.h>

char *string_copy(char v[], char w[]) {
    int i = 0;
    while (w[i]) {
        v[i] = w[i];
        i++;
    }
    v[i] = 0;
}


int main(void) {

    char src_one[] = "";
    char dest_one[] = "Hello World";

    string_copy(dest_one, src_one);
    printf("%s\n", dest_one);


    char src_two[] = "Hallo schöne Welt";
    char dest_two[] = "Hello World";

    string_copy(dest_two, src_two);
    printf("%s\n", dest_two);


    char src_three[] = "Hallo";
    char dest_three[] = "Hello World";

    string_copy(dest_three, src_three);
    printf("%s\n", dest_three);
}
```

### b)
```c
#include <stdio.h>

int string_indexof(char v[], char c)
{
	int i = 0;
	for (; v[i]; i++) {
		if (v[i] == c) {
			return i;
		}
	}
	return -1;
}

int main(void)
{
	printf("%i\n", string_indexof("", 'c'));
	printf("%i\n", string_indexof("Hello World", 'r'));
	printf("%i\n", string_indexof("Hello World", 'o'));
	printf("%i\n", string_indexof("Hello World", 'z'));
	printf("%i\n", string_indexof("Hello World", ' '));
}
```

## Aufgabe 23+24
### a)
```c
#include <stdio.h>
#include <string.h>

#define SMAX 15

int main(int argc, char *argv[])
{
	if (argc < 3) {
		printf("You must supply at least 2 arguments, not %i\n",
		       argc - 1);
		return 1;
	}

	int i = 1;
	for (; i < argc; i++) {
		if (strcmp(argv[i], argv[i + 1]) > 0) {
			printf("Argument %i is the first argument, that is not in lexicographic order\n",
			       (i + 1));
			i = -1;
			break;
		}
	}
	if (i != -1) {
		printf("All arguments are in lexicographic order\n");
	}

	i = 1;
	char v[SMAX] = "";
	for (; i < argc && strlen(v) + strlen(argv[i]) <= SMAX; i++) {
		strcat(v, argv[i]);
	}

	printf("%s\n", v);
}
```

### b+c)
```c
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define VMAX 15

int is_prime(int x, int p[])
{
	if (x == 2) {
		return 1;
	}
	if (x == 1) {
		return 0;
	}
	int i;
	for (i = 0; p[i]; i++) {
		if (x % p[i] == 0) {
			return 0;
		}
	}
	return 1;
}

int count_non_zero(int v[], int n)
{
	int i = 0;
	int result = 0;
	for (; i < n; i++) {
		result += (v[i]) ? 1 : 0;
	}
	return result;
}

void print_array(int arr[], int size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]");
}

int main(int argc, char *argv[])
{
	int primes[VMAX];
	int j = 0;
	for (; j < VMAX; j++) {
		primes[j] = 0;
	}

	if (argc != 2) {
		printf("You must supply 1 argument, not %i\n", argc - 1);
		return 1;
	}

	if (strlen(argv[1]) > 0 && argv[1][0] != '0' &&
	    strspn(argv[1], "0123456789") == strlen(argv[1])) {
		int n = atoi(argv[1]);
		if (n < 1) {
			printf("The passed number is to small (%i)\n", n);
			return 1;
		} else if (n > VMAX) {
			printf("The passed number is to big (%i)\n", n);
			return 1;
		}
		int x, i;
		i = 0;
		for (x = 2; x < n; x++) {
			if (is_prime(x, primes)) {
				primes[i] = x;
				i++;
			}
		}
		printf("Der Anteil der ersten %i Primzahlen ",
		       count_non_zero(primes, VMAX));
		print_array(primes, VMAX);
		printf(" an den natuerlichen Zahlen bis zur %i ist: %f\n", n,
		       count_non_zero(primes, VMAX) / (double)n);

	} else {
		printf("Argument could not be parsed as int");
		return 1;
	}
}

```