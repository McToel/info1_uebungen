#include <stdio.h>

#define DIM 8
#define DIM_S "8"
#define INPUT_ERROR 1
#define BUFFER_ERROR 2

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_binary(int b[])
{
	int i = 0;
	char c;
	for (; i < DIM; i++) {
		char c = getchar();
		if (c == EOF) {
			printf("Buffererror\n");
			return BUFFER_ERROR;
		} else if (c == '1') {
			b[i] = 1;
		} else if (c == '0') {
			b[i] = 0;
		} else {
			printf("%c is not binary\n", c);
			return INPUT_ERROR;
		}
	}

	return 0;
}

void print_array(int arr[], size_t size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]\n");
}

int main(void)
{
	int b[DIM];
	printf("Enter %i bit binary Number: ", DIM);
	int r = read_binary(b);
	print_array(b, DIM);
}