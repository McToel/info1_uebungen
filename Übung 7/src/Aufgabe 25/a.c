#include <stdio.h>

#define LADENSCHLUSS_H 20
#define LADENSCHLUSS_M 00 + 60 * LADENSCHLUSS_H
#define TIME_ERROR 1

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_time()
{
	int hours;
	int minutes;

	printf("Bitte gebe eine Zeit im Format hh:mm an: ");
	scanf("%i", &hours);
	scanf(":%i", &minutes);

	if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0) {
		return TIME_ERROR;
	}

	int min_til_ladenschluss = LADENSCHLUSS_M - (hours * 60 + minutes);

	/*
    Ich verwende gerne tenäre Operatoren aber hier ergeben sie einfach gar keinen Sinn
    */
	if (min_til_ladenschluss == 0) {
		printf("Der Laden hat in diesem Moment geschlossen\n");
	} else if (min_til_ladenschluss > 0) {
		printf("Der Laden hat noch %i Stunden und %i Minuten geöffnet\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	} else {
		min_til_ladenschluss = -min_til_ladenschluss;
		printf("Der Laden ist seit %i Stunden und %i Minuten geschlossen\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	}

	return 0;
}

int main(void)
{
	if (read_time() == TIME_ERROR) {
		flush_buff();
	}
}