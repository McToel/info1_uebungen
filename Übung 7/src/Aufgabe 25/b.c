#include <stdio.h>

#define DATE_ERROR 1

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_date()
{
	int day;
	int month;

	printf("Bitte gebe ein Datum im Format dd.mm an: ");
	scanf("%i", &day);
	scanf(".%i", &month);

	if (month < 1 || month > 12 || day < 1) {
		return DATE_ERROR;
	}
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		if (day > 31)
			return DATE_ERROR;
		break;
	case 2:
		if (day > 28)
			return DATE_ERROR;
		break;

	default:
		if (day > 30)
			return DATE_ERROR;
		break;
	}

	int days = day;
	days += (month >= 2) ? 31 : 0;
	days += (month >= 3) ? 30 : 0;
	days += (month >= 4) ? 31 : 0;
	days += (month >= 5) ? 30 : 0;
	days += (month >= 6) ? 31 : 0;
	days += (month >= 7) ? 31 : 0;
	days += (month >= 8) ? 30 : 0;
	days += (month >= 9) ? 31 : 0;
	days += (month >= 10) ? 30 : 0;
	days += (month >= 11) ? 31 : 0;

	printf("Das Datum ist der %i. Tag im Jahr\n", days);

	return 0;
}

int main(void)
{
	if (read_date() == DATE_ERROR) {
		printf("Datumserror\n");
		flush_buff();
	}
}