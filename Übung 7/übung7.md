# Übungsblatt 7
## Aufgabe 25
### a)
```c
#include <stdio.h>

#define LADENSCHLUSS_H 20
#define LADENSCHLUSS_M 00 + 60 * LADENSCHLUSS_H
#define TIME_ERROR 1

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_time()
{
	int hours;
	int minutes;

	printf("Bitte gebe eine Zeit im Format hh:mm an: ");
	scanf("%i", &hours);
	scanf(":%i", &minutes);

	if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0) {
		return TIME_ERROR;
	}

	int min_til_ladenschluss = LADENSCHLUSS_M - (hours * 60 + minutes);

	/*
    Ich verwende gerne tenäre Operatoren aber hier ergeben sie einfach gar keinen Sinn
    */
	if (min_til_ladenschluss == 0) {
		printf("Der Laden hat in diesem Moment geschlossen\n");
	} else if (min_til_ladenschluss > 0) {
		printf("Der Laden hat noch %i Stunden und %i Minuten geöffnet\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	} else {
		min_til_ladenschluss = -min_til_ladenschluss;
		printf("Der Laden ist seit %i Stunden und %i Minuten geschlossen\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	}

	return 0;
}

int main(void)
{
	if (read_time() == TIME_ERROR) {
		flush_buff();
	}
}
```

### b)
```c
#include <stdio.h>

#define DATE_ERROR 1

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_date()
{
	int day;
	int month;

	printf("Bitte gebe ein Datum im Format dd.mm an: ");
	scanf("%i", &day);
	scanf(".%i", &month);

	if (month < 1 || month > 12 || day < 1) {
		return DATE_ERROR;
	}
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		if (day > 31)
			return DATE_ERROR;
		break;
	case 2:
		if (day > 28)
			return DATE_ERROR;
		break;

	default:
		if (day > 30)
			return DATE_ERROR;
		break;
	}

	int days = day;
	days += (month >= 2) ? 31 : 0;
	days += (month >= 3) ? 30 : 0;
	days += (month >= 4) ? 31 : 0;
	days += (month >= 5) ? 30 : 0;
	days += (month >= 6) ? 31 : 0;
	days += (month >= 7) ? 31 : 0;
	days += (month >= 8) ? 30 : 0;
	days += (month >= 9) ? 31 : 0;
	days += (month >= 10) ? 30 : 0;
	days += (month >= 11) ? 31 : 0;

	printf("Das Datum ist der %i. Tag im Jahr\n", days);

	return 0;
}

int main(void)
{
	if (read_date() == DATE_ERROR) {
		printf("Datumserror\n");
		flush_buff();
	}
}
```

### c)
```c
#include <stdio.h>

#define LADENSCHLUSS_H 20
#define LADENSCHLUSS_M 00 + 60 * LADENSCHLUSS_H
#define TIME_ERROR 1
#define DATE_ERROR 1

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_time()
{
	int hours;
	int minutes;

	printf("Bitte gebe eine Zeit im Format hh:mm an: ");
	scanf("%i", &hours);
	scanf(":%i", &minutes);

	if (hours > 23 || hours < 0 || minutes > 59 || minutes < 0) {
		return TIME_ERROR;
	}

	int min_til_ladenschluss = LADENSCHLUSS_M - (hours * 60 + minutes);

	/*
    Ich verwende gerne tenäre Operatoren aber hier ergeben sie einfach gar keinen Sinn
    */
	if (min_til_ladenschluss == 0) {
		printf("Der Laden hat in diesem Moment geschlossen\n");
	} else if (min_til_ladenschluss > 0) {
		printf("Der Laden hat noch %i Stunden und %i Minuten geöffnet\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	} else {
		min_til_ladenschluss = -min_til_ladenschluss;
		printf("Der Laden ist seit %i Stunden und %i Minuten geschlossen\n",
		       min_til_ladenschluss / 60, min_til_ladenschluss % 60);
	}

	return 0;
}

int read_date()
{
	int day;
	int month;

	printf("Bitte gebe ein Datum im Format dd.mm an: ");
	scanf("%i", &day);
	scanf(".%i", &month);

	if (month < 1 || month > 12 || day < 1) {
		return DATE_ERROR;
	}
	switch (month) {
	case 1:
	case 3:
	case 5:
	case 7:
	case 8:
	case 10:
	case 12:
		if (day > 31)
			return DATE_ERROR;
		break;
	case 2:
		if (day > 28)
			return DATE_ERROR;
		break;

	default:
		if (day > 30)
			return DATE_ERROR;
		break;
	}

	int days = day;
	days += (month >= 2) ? 31 : 0;
	days += (month >= 3) ? 30 : 0;
	days += (month >= 4) ? 31 : 0;
	days += (month >= 5) ? 30 : 0;
	days += (month >= 6) ? 31 : 0;
	days += (month >= 7) ? 31 : 0;
	days += (month >= 8) ? 30 : 0;
	days += (month >= 9) ? 31 : 0;
	days += (month >= 10) ? 30 : 0;
	days += (month >= 11) ? 31 : 0;

	printf("Das Datum ist der %i. Tag im Jahr\n", days);

	return 0;
}

int main(void)
{
	printf("Ladenschließzeitberechnung (t) oder Tag des Jahres Berechnung (d): ");
	char c;
	scanf("%c", &c);

	if (c == 't') {
		if (read_time() == TIME_ERROR) {
			flush_buff();
		}
	} else if (c == 'd') {
		if (read_date() == DATE_ERROR) {
			printf("Datumserror\n");
		}
	} else {
		printf("Gebe das nächste mal doch entweder t oder d ein und nicht %c\n",
		       c);
	}
}
```


## Aufgabe 26
### a)
```c
#include <stdio.h>

#define DIM 8
#define DIM_S "8"
#define INPUT_ERROR 1
#define BUFFER_ERROR 2

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int read_binary(int b[])
{
	int i = 0;
	char c;
	for (; i < DIM; i++) {
		char c = getchar();
		if (c == EOF) {
			printf("Buffererror\n");
			return BUFFER_ERROR;
		} else if (c == '1') {
			b[i] = 1;
		} else if (c == '0') {
			b[i] = 0;
		} else {
			printf("%c is not binary\n", c);
			return INPUT_ERROR;
		}
	}

	return 0;
}

void print_array(int arr[], size_t size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]\n");
}

int main(void)
{
	int b[DIM];
	printf("Enter %i bit binary Number: ", DIM);
	int r = read_binary(b);
	print_array(b, DIM);
}
```