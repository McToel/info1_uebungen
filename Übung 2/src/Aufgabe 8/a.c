#include <stdio.h>

// s1 -> x
int sign(int x)
{
	if (x == 0) {
		return 0;
	} else if (x < 0) {
		return -1;
	} else {
		return 1;
	}
}

int main()
{
	printf("%i\n", sign(2));
	printf("%i\n", sign(0));
	printf("%i\n", sign(-3));
}