#include <stdio.h>

// 3*2 + 2*2 + 1*2 + 0*2,
// aber ich habe keine Ahnung,
// was das für eine Reihe sein soll

// s1 -> x
int eine_komische_reihe(int x)
{
	int result = 0;
	int sumand = 2;
	while (1) {
		if (x == 0) {
			return result;
		}
		x -= 1;
		result += sumand;
		sumand += 2;
	}
}

int main()
{
	printf("%i\n", eine_komische_reihe(0));
	printf("%i\n", eine_komische_reihe(1));
	printf("%i\n", eine_komische_reihe(2));
	printf("%i\n", eine_komische_reihe(3));
	printf("%i\n", eine_komische_reihe(4));
	printf("%i\n", eine_komische_reihe(5));
}