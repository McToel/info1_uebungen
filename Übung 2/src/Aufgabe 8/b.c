#include <stdio.h>

// s1 -> x, s2 -> y
int mod(int x, int y)
{
	int tmp = 0;
	while (1) {
		tmp = x - y;
		if (tmp < 0) {
			return x;
		}
		x -= y;
	}
}

int main()
{
	printf("%i\n", mod(4, 5));
	printf("%i\n", mod(10, 5));
	printf("%i\n", mod(7, 5));
	printf("%i\n", mod(16, 5));
}