# Übungsaufgaben 2

## Aufgabe 5

### a)

#### Fetch:
- Schreibe den Inhalt P1 von PC nach AM und lade den Befehl aus
dem Speicherwerk von P1 über RM nach IR.
- Erhöhe den Befehlszähler PC um 1.

#### Decode:
- Erkenne, dass es sich um eine Initialisierung des Inhalts an der Adresse S2 handelt.
- (E) Lade Inhalt von S2 über RM in die AR für die Eingabe der Initialisierungs-Funktionseinheit.

#### Execute:
- (V) Führe Initialisierung aus.

#### Write-Back:
- (A) Gib das Ergebnis von AR für die Ausgabe der Initialisierungs-Funktionseinheit über WM nach S2 aus.



### b)

#### Fetch:
- Schreibe den Inhalt P3 von PC nach AM und lade den Befehl aus dem Speicherwerk von P3 über RM nach IR.
- Erhöhe den Befehlszähler PC um 1.

#### Decode:
- Erkenne, dass es sich um eine Addition der Inhalte an den Adressen S2 und S1 handelt.
- (E) Lade Inhalt von S2 und S1 einzeln über RM in die AR für die Eingabe der Additions-Funktionseinheit.

#### Execute:
- (V) Führe Additions-Operation aus

#### Write-Back:
- (A) Gib das Ergebnis von AR für die Ausgabe der Additions-Funktionseinheit über WM nach S2 aus.

### b)	Fetch-Schritt von P1

- Steuerwerk schreibt Inhalt von P1 von PC nach AM 
- Steuerwerk setzt D auf Lesen (0)
- Steuerwerk sendet A 
- Speicherwerk liest Adresse von AM (also S2)
- Speicherwerk schickt Wert von (S2) an RM 
- Speicherwert sendet T


### c)	Write-Back-Schritt von P3

- Rechenwerk schickt Wert von AR nach WM 
- Steuerwerk stellt D auf schreiben (1) 
- Steuerwerk schreibt Adresse (S2) von PC nach AM 
- Rechenwerk schickt A
- Speicherwerk liest Speicherort (S2)  von AM 
- Speicherwerk holt Wert von WM 
- Speicherwerk schreibt Wert von AM auf Speicherort (S2) aus AM
- Speicherwerk schickt T


### c)

#### Fetch:
- Schreibe den Inhalt P2 von PC nach AM und lade den Befehl aus dem Speicherwerk von P2 über RM nach IR.
- Erhöhe den Befehlszähler PC um 1.

#### Decode:
- Erkenne, dass es sich um eine bedingter Sprung-Funktion bei Wert 0 des Inhalts an der Adresse S1 nach P6 handelt.
- (E) Lade Inhalt von S1 über RM in die AR für die Eingabe der SPRUNG0-Funktionseinheit.

#### Execute:
- (V) Führe SPRUNG0-Operation aus nach P6, wenn S1 = 0. Im Zweifelsfall PC aktualisieren.


## Aufgabe 6

### a)

1. Steuerwerk setzt D auf Lesen (0).
2. Steuerwerk sendet A.
3. Speicherwerk liest Adresse von AM (also S2).
4. Speicherwerk liest Wert von S2.
5. Speicherwerk sendet den Inhalt von S2 nach RM.
6. Speicherwerk sendet T.
7. Rechenwerk sendet den Inhalt (S2) nach AR und IR.
8. Das Steuerwerk weist das Rechenwerk an Wert von RM nach AR zu schicken.

## Aufgabe 7
### a)
Das Program gibt folgendes zurück:
$$
\sum_{i=0}^{S1}{S1 - i}
$$

Also z.B.:
S1 = 4

4 + 3 + 2 + 1 + 0

### b)

`int a` -> `S1`
`int b` -> `S2`

- P2 bis P5: `a * 5` <- Hier spare ich ein paar CPU cycles, indem ich nicht 5 Mal S1 auf S3 addiere sondern S1 * 2 auf S1 * 2 addiere.
- P6: `(a * 5) + 1`
- P7: `(a * 5 + 1) + 1`
- P8: `(a * 5 + 1 + 1) - b`
- P9: Vergleiche ob die Rechnung Null ergiebt.
```
P1: INIT S3
P2: ADD S3, S1
P3: ADD S3, S1
P4: ADD S3, S3
P5: ADD S3, S1
P6: INC S3
P7: SUB S3, S2
P8: SPRUNG0 PX, S3
P9: INIT S3
P10: RÜCKGABE S3
P11: INC S3
P12: RÜCKGABE S3
```

### c)
Damit bei `char c` eine Ziffer ist, muss `c - '9' - 1` negativ sein und `c - '0'` positiv. Diese Überprüfungen macht das folgende Program
```
P1: SUB S1, D1
P2: SPRUNGNEG P3, S1
P3: RÜCKGABE D3
P4: ADD S1, D2
P5: SPRUNGNEG P3, S1
P6: RÜCKGABE D4
D1: 58
D2: 11
D3: 0
D4: 1
```

## Aufgabe 8
### a)
```c
#include <stdio.h>

// s1 -> x
int sign(int x)
{
	if (x == 0) {
		return 0;
	} else if (x < 0) {
		return -1;
	} else {
		return 1;
	}
}

int main()
{
	printf("%i\n", sign(2));
	printf("%i\n", sign(0));
	printf("%i\n", sign(-3));
}
```

### b)
```c
#include <stdio.h>

// s1 -> x, s2 -> y
int mod(int x, int y)
{
	int tmp = 0;
	while (1) {
		tmp = x - y;
		if (tmp < 0) {
			return x;
		}
		x -= y;
	}
}

int main()
{
	printf("%i\n", mod(4, 5));
	printf("%i\n", mod(10, 5));
	printf("%i\n", mod(7, 5));
	printf("%i\n", mod(16, 5));
}
```

### c)
```c
#include <stdio.h>

// 3*2 + 2*2 + 1*2 + 0*2,
// aber ich habe keine Ahnung,
// was das für eine Reihe sein soll

// s1 -> x
int eine_komische_reihe(int x)
{
	int result = 0;
	int sumand = 2;
	while (1) {
		if (x == 0) {
			return result;
		}
		x -= 1;
		result += sumand;
		sumand += 2;
	}
}

int main()
{
	printf("%i\n", eine_komische_reihe(0));
	printf("%i\n", eine_komische_reihe(1));
	printf("%i\n", eine_komische_reihe(2));
	printf("%i\n", eine_komische_reihe(3));
	printf("%i\n", eine_komische_reihe(4));
	printf("%i\n", eine_komische_reihe(5));
}
```