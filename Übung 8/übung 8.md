# Übungsblatt 8
## Aufgabe 29
### a)
```c
#include <stdio.h>
#include <ctype.h>
#include <limits.h>

int flush(void)
{
	char c;
	while ((c = getchar()) != '\n') {
	}
	return c;
}

int main(void)
{
	int a;
	int b;
	int ergebnis;
	int status;
	printf("Bitte geben Sie ihr Rechnung a + b ein:\n");
	status = scanf("%i+%i", &a, &b);
	if (status == 2) {
		ergebnis = a + b;
		printf("%i + %i = %i", a, b, ergebnis);
		return 0;
		printf("Die Eingabe ist ungueltig.\n");
	}
	flush();
	return 0;
}

/*        
        printf("a:%i b:%i\n", a,b);
        printf("isdigit %i, %i\n", isdigit(a), isdigit(b));
        if (isdigit(a) == 1 && isdigit(b) == 1) {
                ergebnis = a + b;
                printf("%i + %i = %i", a, b, ergebnis);
                return 0;
        }
*/
```


### b+c)
```c
#include <stdio.h>
#include <ctype.h>
#include <limits.h>

int ans = 0;

int flush(void)
{
	char c;
	while ((c = getchar()) != '\n') {
	}
	return c;
}

int rechner()
{
	int a, b, ergebnis, status;
	printf("Bitte geben Sie ihre Rechnung a + b ein:\nMit 'ans' kann mit dem vorherigen Ergebnis gerechnet werden.\n");
	status = scanf("%i+%i", &a, &b);
	if (status == 0) {
		if (scanf("ans+%i", &b) == 1) {
			a = ans;
		} else {
			printf("Die Eingabe ist ungueltig.\n\n");
			flush();
			return 0;
		}
	} else if (status != 2 && status != 0) {
		printf("Die Eingabe ist ungueltig.\n\n");
		flush();
		return 0;
	}
	ergebnis = a + b;
	printf("%i + %i = %i\n", a, b, ergebnis);
	ans = ergebnis;
	return 0;
}

int main(void)
{
	int c;
	printf("Zum Starten einer Rechnung bitte 'n' eingeben!\nZum Beenden des Rechners bitte 'e' eingeben!\n");
	while (1) {
		c = getchar();
		if (c == 'n' || c == 'N') {
			rechner();
			printf("Zum Starten einer neuen Rechnung bitte 'n' eingeben!\nZum Beenden des Rechners bitte 'e' eingeben!\n");
			continue;
		} else if (c == 'e' || c == 'E') {
			flush();
			return 0;
		}
	}
	return 0;
}

/*
Aufgabe 29c:
Die if-Abfrage ab Zeile 19 müsste noch um den Fall status == 1 erweitert werden.
Dieser if-Fall muss dann noch eine if-Abfrage folgen, in der if (scanf("*%i", &b) == 1), else if (scanf("/%i", &b) == 1), else if (scanf("-%i", &b) == 1) abgefragt wird. 
Dann müsste eben genau die zutreffende Rechenoperation umgesetzt werden. 
Somit muss auch die ergebnis-Rechnung, sowie die printf-Ausgabe aus Zeilen 32 und 33 in die if-Abfragen verschoben werden. 
Außerdem muss die if-Funktion ab Zeile 20 auch noch um die weiteren Rechenoperatoren-Abfrage aus Zeile 61 ergänzt werden.
Des Weiteren müsste der else if-Fall in Zeile 27 um den Fall status != 1 ergänzt werden, auch wenn sie dann nahezu unnötig werden dürfte. 
*/
```


## Aufgabe 30 
### a.c
```c
#include "bino.h"
#include <stdio.h>

#define N_ROWS 10

int main(void)
{
	printf("bino(3, 3) = %lu\n", bino(5, 2));

	/*
    check how big i can be without getting an overflow
    result: i should not be larger than 22
    */
	unsigned long i = 1;
	unsigned long last_result = 0;
	for (; i < 25; i++) {
		if (fak(i) < last_result) {
			printf("incorrekt i %lu\n", i);
		}
		last_result = fak(i);
		printf("fak(%lu) = %lu\n", i, last_result);
	}

	unsigned long n = 1, k;
	unsigned long sum = 0;
	for (; n < N_ROWS; n++) {
		for (k = 0; k <= n; k++) {
			printf("\t%lu", bino(n, k));
			sum += bino(n, k);
		}
		printf("\t sum = %lu\n", sum);
		sum = 0;
		/*
        Die Summe ist die 2er-Potenz
        */
	}
}
```

### bino.c
```c
unsigned long fak(unsigned long x)
{
	unsigned long i = 0;
	unsigned long result = 1;
	for (; i < x; i++) {
		result *= x - i;
	}
	return result;
}

unsigned long bino(unsigned long n, unsigned long k)
{
	return (fak(n) / (fak(n - k) * fak(k)));
}
```

### bino.h
```c
#pragma once

unsigned long fak(unsigned long x);
unsigned long bino(unsigned long n, unsigned long k);
```


## Aufgabe 31
### main.c
```c
#include <stdio.h>
#include "km_nautical.h"

#define MAX_SEGMENTS 3

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int main(void)
{
	char option;
	double input;
	double inputs[MAX_SEGMENTS];
	int i;

	while (1) {
		printf("Options:\n");
		printf(" - n (convert km to nautical miles)\n");
		printf(" - k (convert nautical miles to km)\n");
		printf(" - l (enter %i segment lengths and compute their total length)\n",
		       MAX_SEGMENTS);
		printf(" - a (enter %i segment lengths and compute their average length)\n",
		       MAX_SEGMENTS);
		printf("Please enter character: ");
		if (scanf("%c", &option) == EOF) {
			return 0;
		} else if (option == 'n') {
			printf("Enter number to convert: ");
			if (scanf("%lf", &input) == EOF) {
				return INVALID_INPUT_ERROR;
			}
			printf("nautical miles: %lf\n", KM_TO_NAUTICAL(input));
		} else if (option == 'k') {
			printf("Enter number to convert: ");
			if (scanf("%lf", &input) == EOF) {
				return INVALID_INPUT_ERROR;
			}
			printf("km: %lf\n", NAUTICAL_TO_KM(input));
		} else if (option == 'l') {
			printf("Enter subsections:\n");
			for (i = 0; i < MAX_SEGMENTS; i++) {
				if (read_subsection(inputs, i) ==
				    INVALID_INPUT_ERROR) {
					return INVALID_INPUT_ERROR;
				}
			}
			printf("totla lengths: %lf\n",
			       sum(inputs, MAX_SEGMENTS));
		} else if (option == 'a') {
			printf("Enter subsections:\n");
			for (i = 0; i < MAX_SEGMENTS; i++) {
				if (read_subsection(inputs, i) ==
				    INVALID_INPUT_ERROR) {
					return INVALID_INPUT_ERROR;
				}
			}
			printf("average lengths: %lf\n",
			       mean(inputs, MAX_SEGMENTS));
		}
		flush_buff();
	}
}
```

### km_nautical.c
```c
#include <stdio.h>
#include "km_nautical.h"

// #define INVALID_INPUT_ERROR 1
// #define KM_TO_NAUTICAL (km) km * 0.53996
// #define NAUTICAL_TO_KM (nautical) nautical * 1.852

double sum(double x[], int size)
{
	int i;
	double result = 0;
	for (int i = 0; i < size; i++) {
		result += x[i];
	}
	return result;
}

double mean(double x[], int size)
{
	return sum(x, size) / (double)size;
}

int read_subsection(double result[], int index)
{
	double value = 0;
	if (scanf("%lf", &value) == EOF) {
		return INVALID_INPUT_ERROR;
	}
	result[index] = value;
	return 0;
}
```

### km_nautical.h
```c
#pragma once

#define INVALID_INPUT_ERROR 1
#define KM_TO_NAUTICAL(km) km * 0.53996
#define NAUTICAL_TO_KM(nautical) nautical * 1.852

double sum(double x[], int size);
double mean(double x[], int size);
int read_subsection(double result[], int index);
```