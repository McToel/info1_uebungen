#include "bino.h"
#include <stdio.h>

#define N_ROWS 10

int main(void)
{
	printf("bino(3, 3) = %lu\n", bino(5, 2));

	/*
    check how big i can be without getting an overflow
    result: i should not be larger than 22
    */
	unsigned long i = 1;
	unsigned long last_result = 0;
	for (; i < 25; i++) {
		if (fak(i) < last_result) {
			printf("incorrekt i %lu\n", i);
		}
		last_result = fak(i);
		printf("fak(%lu) = %lu\n", i, last_result);
	}

	unsigned long n = 1, k;
	unsigned long sum = 0;
	for (; n < N_ROWS; n++) {
		for (k = 0; k <= n; k++) {
			printf("\t%lu", bino(n, k));
			sum += bino(n, k);
		}
		printf("\t sum = %lu\n", sum);
		sum = 0;
		/*
        Die Summe ist die 2er-Potenz
        */
	}
}