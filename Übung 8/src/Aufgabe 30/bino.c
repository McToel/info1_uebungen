unsigned long fak(unsigned long x)
{
	unsigned long i = 0;
	unsigned long result = 1;
	for (; i < x; i++) {
		result *= x - i;
	}
	return result;
}

unsigned long bino(unsigned long n, unsigned long k)
{
	return (fak(n) / (fak(n - k) * fak(k)));
}
