#include <stdio.h>
#include <ctype.h>
#include <limits.h>

int ans = 0;

int flush(void)
{
	char c;
	while ((c = getchar()) != '\n') {
	}
	return c;
}

int rechner()
{
	int a, b, ergebnis, status;
	printf("Bitte geben Sie ihre Rechnung a + b ein:\nMit 'ans' kann mit dem vorherigen Ergebnis gerechnet werden.\n");
	status = scanf("%i+%i", &a, &b);
	if (status == 0) {
		if (scanf("ans+%i", &b) == 1) {
			a = ans;
		} else {
			printf("Die Eingabe ist ungueltig.\n\n");
			flush();
			return 0;
		}
	} else if (status != 2 && status != 0) {
		printf("Die Eingabe ist ungueltig.\n\n");
		flush();
		return 0;
	}
	ergebnis = a + b;
	printf("%i + %i = %i\n", a, b, ergebnis);
	ans = ergebnis;
	return 0;
}

int main(void)
{
	int c;
	printf("Zum Starten einer Rechnung bitte 'n' eingeben!\nZum Beenden des Rechners bitte 'e' eingeben!\n");
	while (1) {
		c = getchar();
		if (c == 'n' || c == 'N') {
			rechner();
			printf("Zum Starten einer neuen Rechnung bitte 'n' eingeben!\nZum Beenden des Rechners bitte 'e' eingeben!\n");
			continue;
		} else if (c == 'e' || c == 'E') {
			flush();
			return 0;
		}
	}
	return 0;
}

/*
Aufgabe 29c:
Die if-Abfrage ab Zeile 19 müsste noch um den Fall status == 1 erweitert werden.
Dieser if-Fall muss dann noch eine if-Abfrage folgen, in der if (scanf("*%i", &b) == 1), else if (scanf("/%i", &b) == 1), else if (scanf("-%i", &b) == 1) abgefragt wird. 
Dann müsste eben genau die zutreffende Rechenoperation umgesetzt werden. 
Somit muss auch die ergebnis-Rechnung, sowie die printf-Ausgabe aus Zeilen 32 und 33 in die if-Abfragen verschoben werden. 
Außerdem muss die if-Funktion ab Zeile 20 auch noch um die weiteren Rechenoperatoren-Abfrage aus Zeile 61 ergänzt werden.
Des Weiteren müsste der else if-Fall in Zeile 27 um den Fall status != 1 ergänzt werden, auch wenn sie dann nahezu unnötig werden dürfte. 
*/