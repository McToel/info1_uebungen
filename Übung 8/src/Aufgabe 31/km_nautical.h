#pragma once

#define INVALID_INPUT_ERROR 1
#define KM_TO_NAUTICAL(km) km * 0.53996
#define NAUTICAL_TO_KM(nautical) nautical * 1.852

double sum(double x[], int size);
double mean(double x[], int size);
int read_subsection(double result[], int index);