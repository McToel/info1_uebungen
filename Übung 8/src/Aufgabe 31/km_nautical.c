#include <stdio.h>
#include "km_nautical.h"

// #define INVALID_INPUT_ERROR 1
// #define KM_TO_NAUTICAL (km) km * 0.53996
// #define NAUTICAL_TO_KM (nautical) nautical * 1.852

double sum(double x[], int size)
{
	int i;
	double result = 0;
	for (int i = 0; i < size; i++) {
		result += x[i];
	}
	return result;
}

double mean(double x[], int size)
{
	return sum(x, size) / (double)size;
}

int read_subsection(double result[], int index)
{
	double value = 0;
	if (scanf("%lf", &value) == EOF) {
		return INVALID_INPUT_ERROR;
	}
	result[index] = value;
	return 0;
}