#include <stdio.h>
#include "km_nautical.h"

#define MAX_SEGMENTS 3

int flush_buff(void)
{
	int c = getchar();
	while (c != '\n' && c != EOF) {
		c = getchar();
	}
	return c != EOF; /* wahr, wenn c == '\n' und falsch wenn c == EOF */
}

int main(void)
{
	char option;
	double input;
	double inputs[MAX_SEGMENTS];
	int i;

	while (1) {
		printf("Options:\n");
		printf(" - n (convert km to nautical miles)\n");
		printf(" - k (convert nautical miles to km)\n");
		printf(" - l (enter %i segment lengths and compute their total length)\n",
		       MAX_SEGMENTS);
		printf(" - a (enter %i segment lengths and compute their average length)\n",
		       MAX_SEGMENTS);
		printf("Please enter character: ");
		if (scanf("%c", &option) == EOF) {
			return 0;
		} else if (option == 'n') {
			printf("Enter number to convert: ");
			if (scanf("%lf", &input) == EOF) {
				return INVALID_INPUT_ERROR;
			}
			printf("nautical miles: %lf\n", KM_TO_NAUTICAL(input));
		} else if (option == 'k') {
			printf("Enter number to convert: ");
			if (scanf("%lf", &input) == EOF) {
				return INVALID_INPUT_ERROR;
			}
			printf("km: %lf\n", NAUTICAL_TO_KM(input));
		} else if (option == 'l') {
			printf("Enter subsections:\n");
			for (i = 0; i < MAX_SEGMENTS; i++) {
				if (read_subsection(inputs, i) ==
				    INVALID_INPUT_ERROR) {
					return INVALID_INPUT_ERROR;
				}
			}
			printf("totla lengths: %lf\n",
			       sum(inputs, MAX_SEGMENTS));
		} else if (option == 'a') {
			printf("Enter subsections:\n");
			for (i = 0; i < MAX_SEGMENTS; i++) {
				if (read_subsection(inputs, i) ==
				    INVALID_INPUT_ERROR) {
					return INVALID_INPUT_ERROR;
				}
			}
			printf("average lengths: %lf\n",
			       mean(inputs, MAX_SEGMENTS));
		}
		flush_buff();
	}
}