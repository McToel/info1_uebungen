#include <stdio.h>
#include <math.h>

double decode_floating_point(int floating_point[], int k, int n)
{
	double mantisse = 1;
	for (int i = 0; i < k - 1; i++) {
		mantisse +=
			floating_point[i] * pow(2.0, (double)(-(k - 1 - i)));
	}

	int q = pow(2.0, (double)(n - k - 1)) - 1;
	int exponent = 0;
	for (int i = k - 1, e = 0; i < n - 1; i++, e++) {
		exponent += floating_point[i] * pow(2.0, (double)e);
	}
	exponent -= q;

	double result = mantisse * pow(2.0, (double)exponent);
	if (floating_point[n - 1])
		return -result;
	else
		return result;
}

int main(void)
{
	int number_12[] = { 0, 0, 0, 1, 0, 1, 1, 0 };
	printf("%f\n", decode_floating_point(number_12, 5, 8));

	int number_negative_14_point_5[] = { 1, 0, 1, 1, 0, 1, 1, 1 };
	printf("%f\n", decode_floating_point(number_negative_14_point_5, 5, 8));
}