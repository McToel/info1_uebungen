# Aufgabe 20
## a)
 1. $\frac{1}{2^{10}} < \frac{1}{1000}$ -> k = 10
 2. $\frac{1}{2^{16}}$
 3. $\frac{2^{-3}}{2^1} = \frac{0.002}{2}$ -> k >= 1
 4. 
$k = 5, n = 8$

$m * 2^e = 10.4 * 2^0 = 5.2 * 2^1 = 2.6 * 2^2 = 1.3 2^3$

Exponent Excess q 3-bit:
$q = 2^{n-k-1}-1 = 2^{2}-1 = 3$

$C_{EX-3,3}(3) = 3 + 3 = (110)_{EX-3,3}$

$C_{FK-4,4}(0.3) = C_{2,4}(rd(2^4*0.3)) = C_{2,4}(rd(4.8)) = C_{2,4}(5)$

1.0101 * 2^3 = 1010.1 = 10.5

Rundudsfehler: 0.1

## b)
### 1.
12 + 0.25

k = 5, n = 8

12 = 12 * 2⁰ = 6 * 2¹ = 1.5 * 2³

0.25 = 0.25 * 2⁰ = 1 * 2⁻²

m
0.5 * 2⁴ = 8 = 1000

0 * 2⁴ = 0 = 0000

1.1000 * 2³ + 1.0000 * 2⁻² = 1.1000 * 2³ + 0.00001 * 2³ = 1.10001 * 2³

Runden: 1.1001 * 2³
-> Ergebniss: 12.5, Rundungsfehler 0.25

### 2.
1.9375 + 3.2

k = 5, n = 8

1.9375 = 1.9375 * 2⁰

3.2 = 1.6 * 2¹

m

0.9375 * 2⁴ = 1.8750 * 2³ = 2.7500 * 2² = 3.5000 * 2¹ = 7 = 0111

0.6 * 2⁴ = 9.6 = 1001

1.0111 * 2⁰ + 1.1001 * 2¹ = (0.10111 + 1.1001) * 2¹ = 10.01001 * 2¹ = 1.001001 * 2²

Runden: 1.0010 * 2² = 100.1 = 4.5

Rundungsfehler: 5.1375 - 4.5 = 0.64

### 3.
r = 1e10

s = 1e-10

## c)
```c
#include <stdio.h>
#include <math.h>

double decode_floating_point(int floating_point[], int k, int n)
{
	double mantisse = 1;
	for (int i = 0; i < k - 1; i++) {
		mantisse +=
			floating_point[i] * pow(2.0, (double)(-(k - 1 - i)));
	}

	int q = pow(2.0, (double)(n - k - 1)) - 1;
	int exponent = 0;
	for (int i = k - 1, e = 0; i < n - 1; i++, e++) {
		exponent += floating_point[i] * pow(2.0, (double)e);
	}
	exponent -= q;

	double result = mantisse * pow(2.0, (double)exponent);
	if (floating_point[n - 1])
		return -result;
	else
		return result;
}

int main(void)
{
	int number_12[] = { 0, 0, 0, 1, 0, 1, 1, 0 };
	printf("%f\n", decode_floating_point(number_12, 5, 8));

	int number_negative_14_point_5[] = { 1, 0, 1, 1, 0, 1, 1, 1 };
	printf("%f\n", decode_floating_point(number_negative_14_point_5, 5, 8));
}
```