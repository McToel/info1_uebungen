#include <stdio.h>
#include <ctype.h>

void manipulate_and_print(int v[], int size)
{
        for (int i = 0; i < size; i++) {
                if (v[i] < 0)
                        printf("\n");
                else if (v[i] % 3 == 0)
                        printf("%i\n", v[i] / 2);
                else if (v[i] % 3 == 1)
                        printf("%i\n", ++v[i]);
                else
                        printf("%i\n", -v[i] - 2);
        }
}

int main(void)
{
        int test_array[] = {-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int size = sizeof(test_array) / sizeof(int);

        manipulate_and_print(test_array, size);

        return 0;
}