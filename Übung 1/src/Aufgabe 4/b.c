#include <stdio.h>
#include <ctype.h>

int array_to_upper(char v[], char w[], int size)
{
        for (int i = 0; i < size; i++) {
                if (!isalpha(v[i]))
                        return 1;
        }
        for (int i = 0; i < size; i++) {
                w[i] = toupper(v[i]);
        }
        return 0;
}

int main(void)
{
        char test_array_one[] = {'H', 'a', 'l', 'l', 'o', 0};
        char test_array_two[] = {87, 101, 108, 116, 0};
        char test_array_three[] = {'I', 'n', 'f', 'o', 'r', 'm', 'a', 't', 'i', 'k', '1', 0};

        char result_one[6];
        if (!array_to_upper(test_array_one, result_one, 5))
                printf("%s\n", result_one);

        char result_two[5];
        if (!array_to_upper(test_array_two, result_two, 4))
                printf("%s\n", result_two);

        char result_three[12];
        if (!array_to_upper(test_array_three, result_three, 11))
                printf("%s\n", result_three);

        return 0;
}