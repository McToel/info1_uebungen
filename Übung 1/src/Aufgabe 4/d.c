#include <stdio.h>
#include <ctype.h>

void copy_and_transform_digits(char v[], char w[], int size)
{
        for (int i = 0; i < size; i++)
        {
                if (isdigit(v[i]))
                        w[i] = 'Z' - v[i] + '0';
                else
                        w[i] = v[i];
        }
}

int main(void)
{
        char test_array[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 0};
#define SIZE 17
        char result_array[SIZE];

        copy_and_transform_digits(test_array, result_array, SIZE);

        printf("%s\n", test_array);
        printf("%s\n", result_array);

        return 0;
}