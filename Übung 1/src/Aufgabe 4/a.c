#include <stdio.h>

int sum(int w[], int size)
{
        int result = 0;
        for (int i = 0; i < size; i++) {
                result += w[i];
        }
        return result;
}

int main(void)
{
        int test_array[] = {1, 2, 3, 4, 5};
        int size = sizeof(test_array) / sizeof(int);

        printf("%i\n", sum(test_array, size));

        return 0;
}
