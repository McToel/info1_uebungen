#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

int main(void)
{
        int n = rand();
        char c = n % 128;

        if (c >= 48 && c <= 57)
                printf("%i\n", 1);
        if (c >= 65 && c <= 90)
                printf("%i\n", 2);
        if (islower(c))
                printf("%i\n", 3);
        if (isxdigit(c))
                printf("%i\n", 4);
        printf("%c\n%i", c, c);

        return 0;
}
