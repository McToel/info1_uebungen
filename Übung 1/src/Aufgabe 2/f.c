#include "stdio.h"

int main(void)
{
        printf("%.10f\n", 7 / 4);
        printf("%.10f\n", 7 / 4.0);

        return 0;
}

// format ‘%f’ expects argument of type ‘double’, but argument 2 has type ‘int’ [-Wformat=]
//
// 7 / 2 ist eine Integer-Division und liefert daher einen Integer.
// Drum gibt es eine Warnung, da %f eigentlich eine floating point
// number erwartet.