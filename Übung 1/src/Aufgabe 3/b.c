#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

int main(void)
{
        int n = rand() % 128;

        for (int i = 0; i <= n; i++) {
                printf("%i\t", i);
                if isalpha(i)
                        printf("Ja\n");
                else
                        printf("Nein\n");
        }

        return 0;
}
