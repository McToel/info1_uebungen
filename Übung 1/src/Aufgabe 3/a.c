#include "stdio.h"

int main(void)
{
        printf("i\ti^2\n");
        for (int i = 0; i < 100; i++)
                printf("%i:\t%i\n", i, i * i);

        return 0;
}