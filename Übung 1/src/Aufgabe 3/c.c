#include <stdio.h>
#include <stdlib.h>

// Dieses Programm malt eine halbe Pyramide aus "00" in die Konsole.
// Die Pyramide ist zwischen 2 und 7 Zeilen hoch.

int main(void)
{
        int i;
        int k;
        int r;
        // Zufallszahl zwischen 2 und 7
        r = rand() % (5) + 2;
        for (i = 0; i < r; i++) {
                for (k = i; k >= 0; k--) {
                        printf("00");
                }
                printf("\n");
        }
        return 0;
}