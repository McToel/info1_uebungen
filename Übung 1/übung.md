Aufgabe 1
## a
```c
int main(void)
{
        return 0;
}
```

## b
```c
#include <stdio.h>

int main(void)
{
        printf("Hallo");
        return 0;
}

```

## c
```c
#include <stdio.h>

int main(void)
{
        int x = 3;
        printf("%i", x);
        return 0;
}
```

## d
```c
#include "stdio.h"
#include "ctype.h"

int main(void)
{
        int n = 5;
        printf("%i\n", n);
        return 0;
}
```

# Aufgabe 2
## a
```c
#include "stdio.h"
#include "stdlib.h"

int main(void)
{
        int k, m, n;
        k = -1;
        m = rand();
        n = k * m;
        printf("%i\n", n);
        return 0;
}
```

## b
```c
#include "stdio.h"
#include "limits.h"

int main(void)
{
        int n;
        n = INT_MIN;
        n--;
        printf("%i\n", n);

        return 0;
}
```

## c
```c
#include "stdio.h"

int main(void)
{
        char c = '?';
        printf("%c\n", c);
        printf("%i\n", c);

        return 0;
}
```

## d
```c
#include "stdio.h"

int main(void)
{
        double x = 4.5 * 10e2;
        printf("%f\n", x);
        printf("%e\n", x);

        return 0;
}
```

## e
```c
#include "stdio.h"

int main(void)
{
        double x = 3.9;
        int k = 3.9;
        x /= 2;
        k /= 2;

        printf("%e\n", x);
        printf("%i\n", k);

        return 0;
}
```

## f
```c
#include "stdio.h"

int main(void)
{
        printf("%.10f\n", 7 / 4);
        printf("%.10f\n", 7 / 4.0);

        return 0;
}

// format ‘%f’ expects argument of type ‘double’, but argument 2 has type ‘int’ [-Wformat=]
//
// 7 / 2 ist eine Integer-Division und liefert daher einen Integer.
// Drum gibt es eine Warnung, da %f eigentlich eine floating point
// number erwartet.
```

## g
```c
#include "stdio.h"
#include "math.h"

int main(void)
{
        printf("%.10f\n", cos(2.0));
        printf("%.10f\n", log(250.0));

        return 0;
}

```

## h
```c
#include "stdio.h"
#include "stdlib.h"

int main(void)
{
        int a = rand(), b = rand(), c = rand();
        printf("%i\n", (a + b) * c);
        printf("%i\n", (a / b) + c);

        return 0;
}
```

## i
```c
#include "stdio.h"
#include "stdlib.h"
#include "limits.h"

int main(void)
{
        int n = -rand();
        double x = (double)n / RAND_MAX;
        printf("%i\n%f\n", n, x);

        return 0;
}

```

## j
```c
#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

int main(void)
{
        int n = rand();
        char c = n % 128;

        if (c >= 48 && c <= 57)
                printf("%i\n", 1);
        if (c >= 65 && c <= 90)
                printf("%i\n", 2);
        if (islower(c))
                printf("%i\n", 3);
        if (isxdigit(c))
                printf("%i\n", 4);
        printf("%c\n%i", c, c);

        return 0;
}
```

# Aufgabe 3
## a
```c
#include "stdio.h"

int main(void)
{
        printf("i\ti^2\n");
        for (int i = 0; i < 100; i++)
                printf("%i:\t%i\n", i, i * i);

        return 0;
}
```

## b
```c
#include "stdio.h"
#include "stdlib.h"
#include "ctype.h"

int main(void)
{
        int n = rand() % 128;

        for (int i = 0; i <= n; i++) {
                printf("%i\t", i);
                if isalpha(i)
                        printf("Ja\n");
                else
                        printf("Nein\n");
        }

        return 0;
}
```

## c
```c
#include <stdio.h>
#include <stdlib.h>

// Dieses Programm malt eine halbe Pyramide aus "00" in die Konsole.
// Die Pyramide ist zwischen 2 und 7 Zeilen hoch.

int main(void)
{
        int i;
        int k;
        int r;
        // Zufallszahl zwischen 2 und 7
        r = rand() % (5) + 2;
        for (i = 0; i < r; i++) {
                for (k = i; k >= 0; k--) {
                        printf("00");
                }
                printf("\n");
        }
        return 0;
}
```

# Aufgabe 4
## a
```c
#include <stdio.h>

int sum(int w[], int size)
{
        int result = 0;
        for (int i = 0; i < size; i++) {
                result += w[i];
        }
        return result;
}

int main(void)
{
        int test_array[] = {1, 2, 3, 4, 5};
        int size = sizeof(test_array) / sizeof(int);

        printf("%i\n", sum(test_array, size));

        return 0;
}

```

## b
```c
#include <stdio.h>
#include <ctype.h>

int array_to_upper(char v[], char w[], int size)
{
        for (int i = 0; i < size; i++) {
                if (!isalpha(v[i]))
                        return 1;
        }
        for (int i = 0; i < size; i++) {
                w[i] = toupper(v[i]);
        }
        return 0;
}

int main(void)
{
        char test_array_one[] = {'H', 'a', 'l', 'l', 'o', 0};
        char test_array_two[] = {87, 101, 108, 116, 0};
        char test_array_three[] = {'I', 'n', 'f', 'o', 'r', 'm', 'a', 't', 'i', 'k', '1', 0};

        char result_one[6];
        if (!array_to_upper(test_array_one, result_one, 5))
                printf("%s\n", result_one);

        char result_two[5];
        if (!array_to_upper(test_array_two, result_two, 4))
                printf("%s\n", result_two);

        char result_three[12];
        if (!array_to_upper(test_array_three, result_three, 11))
                printf("%s\n", result_three);

        return 0;
}
```

## c
```c
#include <stdio.h>
#include <ctype.h>

void manipulate_and_print(int v[], int size)
{
        for (int i = 0; i < size; i++) {
                if (v[i] < 0)
                        printf("\n");
                else if (v[i] % 3 == 0)
                        printf("%i\n", v[i] / 2);
                else if (v[i] % 3 == 1)
                        printf("%i\n", ++v[i]);
                else
                        printf("%i\n", -v[i] - 2);
        }
}

int main(void)
{
        int test_array[] = {-2, -1, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int size = sizeof(test_array) / sizeof(int);

        manipulate_and_print(test_array, size);

        return 0;
}
```

## d
```c
#include <stdio.h>
#include <ctype.h>

void copy_and_transform_digits(char v[], char w[], int size)
{
        for (int i = 0; i < size; i++)
        {
                if (isdigit(v[i]))
                        w[i] = 'Z' - v[i] + '0';
                else
                        w[i] = v[i];
        }
}

int main(void)
{
        char test_array[] = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f', 0};
#define SIZE 17
        char result_array[SIZE];

        copy_and_transform_digits(test_array, result_array, SIZE);

        printf("%s\n", test_array);
        printf("%s\n", result_array);

        return 0;
}
```