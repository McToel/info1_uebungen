# Übungsblatt 4

## Aufgabe 13

### a

1. 75
2. -60
3. -90

### b

1. 42
2. 99
3. -14

### c

| Dezimalzahl | 1K 4-Bit | 2K 4-Bit | 1K 8-Bit  | 2K 8-Bit  |
| ----------- | -------- | -------- | --------- | --------- |
| 7           | 0111     | 0111     | 0000 0111 | 0000 0111 |
| -3          | 1100     | 1011     | 1000 0100 | 1000 0011 |
| -4          | 1011     | 1010     | 1000 0011 | 1000 0010 |
| 8           | -        | -        | 0000 1000 | 0000 1000 |
| 33          | -        | -        | 0010 0001 | 0010 0001 |
| −128        | -        | -        | -         | 1000 0000 |
| -101        | -        | -        | 1001 1010 | 1001 1001 |

## Aufgabe 16
### a
```c
#include <stdio.h>

void print_array(int arr[], size_t size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]\n");
}

void ein_k_add(int a[], int b[], int result[], int len)
{
	int überlauf = 0;
	for (int i = len - 1; i >= 0; i--) {
		result[i] = a[i] + b[i] + überlauf;
		überlauf = result[i] / 2;
		result[i] %= 2;
	}
}

void main(void)
{
	int a[] = { 1, 0, 0, 0 };
	int b[] = { 0, 1, 1, 1 };
	int result[4];

	ein_k_add(a, b, result, 4);

	printf("\n  ");
	print_array(a, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int c[] = { 1, 1, 0, 0 };

	ein_k_add(c, b, result, 4);

	printf("\n  ");
	print_array(c, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int d[] = { 0, 0, 0, 1 };
	int e[] = { 0, 0, 1, 1 };

	ein_k_add(d, e, result, 4);

	printf("\n  ");
	print_array(d, 4);
	printf("+ ");
	print_array(e, 4);
	printf("= ");
	print_array(result, 4);
}
```

### b
```c
#include <stdio.h>

void print_array(int arr[], size_t size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]\n");
}

/*
Damit ein_k_add funktionier, muss das Ergebniss der Rechnung
bei überlauf am Ende noch um 1 erhöht werden
*/

void ein_k_add(int a[], int b[], int result[], int len)
{
	int überlauf = 0;
	for (int i = len - 1; i >= 0; i--) {
		result[i] = a[i] + b[i] + überlauf;
		überlauf = result[i] / 2;
		result[i] %= 2;
	}
	if (überlauf) {
		int eins[len];
		for (int i = 0; i < len - 1; i++) {
			eins[i] = 0;
		}
		eins[len - 1] = 1;
		ein_k_add(result, eins, result, len);
	}
}

void main(void)
{
	int a[] = { 1, 0, 0, 0 };
	int b[] = { 0, 1, 1, 1 };
	int result[4];

	ein_k_add(a, b, result, 4);

	printf("\n  ");
	print_array(a, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int c[] = { 1, 1, 0, 0 };

	ein_k_add(c, b, result, 4);

	printf("\n  ");
	print_array(c, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int d[] = { 0, 0, 0, 1 };
	int e[] = { 0, 0, 1, 1 };

	ein_k_add(d, e, result, 4);

	printf("\n  ");
	print_array(d, 4);
	printf("+ ");
	print_array(e, 4);
	printf("= ");
	print_array(result, 4);
}
```

### c
```c
#include <stdio.h>
#include <stdlib.h>

void main(void)
{
	char c = (rand() % 256);
	printf("c: %i\n", c);

	for (int i = 0; i < 8; i++) {
		if (c & (1 << i)) {
			printf("Bit gesetzt\n");
		} else {
			printf("Bit nicht gesetzt\n");
		}
	}
}
```