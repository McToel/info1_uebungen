#include <stdio.h>
#include <stdlib.h>

void main(void)
{
	char c = (rand() % 256);
	printf("c: %i\n", c);

	for (int i = 0; i < 8; i++) {
		if (c & (1 << i)) {
			printf("Bit gesetzt\n");
		} else {
			printf("Bit nicht gesetzt\n");
		}
	}
}