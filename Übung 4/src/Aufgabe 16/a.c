#include <stdio.h>

void print_array(int arr[], size_t size)
{
	printf("[ ");
	for (int i = 0; i < size; i++) {
		printf("%i, ", arr[i]);
	}
	printf("]\n");
}

void ein_k_add(int a[], int b[], int result[], int len)
{
	int überlauf = 0;
	for (int i = len - 1; i >= 0; i--) {
		result[i] = a[i] + b[i] + überlauf;
		überlauf = result[i] / 2;
		result[i] %= 2;
	}
}

void main(void)
{
	int a[] = { 1, 0, 0, 0 };
	int b[] = { 0, 1, 1, 1 };
	int result[4];

	ein_k_add(a, b, result, 4);

	printf("\n  ");
	print_array(a, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int c[] = { 1, 1, 0, 0 };

	ein_k_add(c, b, result, 4);

	printf("\n  ");
	print_array(c, 4);
	printf("+ ");
	print_array(b, 4);
	printf("= ");
	print_array(result, 4);

	int d[] = { 0, 0, 0, 1 };
	int e[] = { 0, 0, 1, 1 };

	ein_k_add(d, e, result, 4);

	printf("\n  ");
	print_array(d, 4);
	printf("+ ");
	print_array(e, 4);
	printf("= ");
	print_array(result, 4);
}