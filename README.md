# Info1_Übungen
Das hier sind meine Info1 Übungsaufgaben. Diese sind vor allem hier, damit ich und mein Team gemeinsam an den Aufgaben arbeiten und unsere Ergebnisse vergleichen können.


## Abschreiben
Wer auf die Idee kommt, die "Lösungen" abzuschreiben, denkt lieber nochmal darüber nach.
- Du lernst selber ungefähr gar nichts dabei, kannst es also auch gleich lassen
- Du riskierst, dass du und ICH einen Plagiatsvorwurf bekommen, und dann beide unsere Abgaben 0 Punkte geben. Das ist einfach mega asozial mir gegenüber.


## Combine PDFs
```
qpdf --empty --pages *.pdf -- abgabe.pdf
```