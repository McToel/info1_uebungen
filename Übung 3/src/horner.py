def horner(x: int, b: int, ):
    print(x, 'umgewandelt in basis', b)
    y = []
    while x > 0:
        y.append(x % b)
        print(f'{x} % {b} = {x % b}')
        x //= b
        print(f'{x} // {b} = {x // b}')

    print(f'({"".join(map(str, y[::-1]))})_{b}')
    # return ''.join(map(str, y[::-1]))

horner(42, 2)