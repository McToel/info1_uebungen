#include <stdio.h>
#include <ctype.h>

void check_char(char c)
{
	if (!isdigit(c)) {
		printf("'%c' is not a digit\n", c);
	}
	if (isalpha(c) && isupper(c)) {
		printf("'%c' is an uppercase letter\n", c);
		printf("'%c' is the lowercase verions\n", tolower(c));
	}
	if (ispunct(c)) {
		printf("'%c' is a punctuation mark\n", c);
	}
}

int main()
{
	check_char('A');
	check_char(';');
	check_char('3');
}